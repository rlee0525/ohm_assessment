"""increase the point_balance

Revision ID: 29ee12f1c2f3
Revises: 00000000
Create Date: 2017-11-04 18:08:40.744386

"""

# revision identifiers, used by Alembic.
revision = '29ee12f1c2f3'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("UPDATE user SET point_balance=1000 WHERE user_id=2")
    op.execute("UPDATE user SET tier='Bronze' WHERE user_id=3")


def downgrade():
    pass
