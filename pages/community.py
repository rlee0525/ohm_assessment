from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User
from models._helpers import *

@app.route('/community', methods=['GET'])
def community():

    login_user(User.query.get(1))

    points_and_dollars = current_user.get_points_and_dollars()


    args = {
            'points': points_and_dollars['points'],
            'dollars': points_and_dollars['dollars'],
            'gift_card_eligible': True,

            'cashout_ok': True,
            'user_below_silver': current_user.is_below_tier('Silver'),
    }

    users = list(db.engine.execute("SELECT * FROM user ORDER BY create_time DESC LIMIT 5"))
    phones_list = []
    location_list = []

    for user in users:
      phones = list(db.engine.execute("SELECT attribute FROM rel_user_multi WHERE (user_id=%s)" % user.user_id))
      unpacked = []

      for phone in phones:
        unpacked.append(phone)
      
      phones_list.append(unpacked)

      locations = list(db.engine.execute("SELECT attribute FROM rel_user WHERE (user_id=%s)" % user.user_id))
      location_list.append(locations)

    return render_template("community.html", users=users, phones_list=phones_list, location_list=location_list)

